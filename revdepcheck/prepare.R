dir.create('Library', showWarnings = FALSE)
stopifnot(dir.exists('Library'))

# Make sure that package installers are aware of the packages already available
.libPaths(c('Library', .libPaths()))

db <- available.packages(repos = BiocManager::repositories())
# CRAN says to check recursive strong reverse dependencies of strong reverse dependencies and reverse suggests
# revdeps <- tools::package_dependencies('XML', reverse = TRUE, which = "most", recursive = "strong")
# But let's start with strong reverse dependencies
revdeps <- tools::package_dependencies('XML', db = db, reverse = TRUE, which = "strong")$XML

pkgcache <- function(pkgs, db) {
	stopifnot(pkgs %in% rownames(db))
	existing <- Sys.glob('*.tar.gz')

	desired <- db[pkgs,]
	desired <- cbind(
		desired,
		tarball = paste0(pkgs, '_', db[pkgs, 'Version'], '.tar.gz')
	)
	unlink(stale <- setdiff(existing, desired[,'tarball']))
	existing <- intersect(existing, desired[,'tarball'])
	message('Cleaned up stale package[s]: ', length(stale))

	fine <- vapply(existing, function(f) {
		l <- utils::untar(f, list = TRUE)
		is.null(status <- attr(l, 'status')) || identical(status, 0L)
	}, logical(1))
	unlink(existing[!fine])
	existing <- existing[fine]
	message('Cleaned up damaged package[s]: ', sum(!fine))

	todo <- !desired[,'tarball'] %in% existing
	message('Need to download package[s]: ', sum(todo))
	downloaded <- download.packages(
		desired[todo, 'Package'], '.',
		available = db
	)[,2]

	invisible(c(existing, downloaded))
}

# Download the reverse dependencies
pkgcache(revdeps, db)

# Have strong dependencies + Suggests of the packages we'll be running R CMD check on
revdepdeps <- tools::package_dependencies(revdeps, db = db, which = 'most')
BiocManager::install(unique(unlist(revdepdeps)), lib = 'Library')
