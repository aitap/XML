R = R
R-devel = R
PACKAGE = $(shell $(R)script -e '\
 cat(read.dcf("DESCRIPTION")[,c("Package","Version")], sep = "_"); \
 cat(".tar.gz") \
')
REVDEPCHECK_NCPUS = $(shell nproc)
REVDEPCHECK_DIRECTORY = $(shell realpath ../XML_revdepcheck)

-include Makefile.local

all: $(PACKAGE)

.PHONY: check check-cran install revdep-prepare revdep-compare

$(PACKAGE): man/* R/* inst/* src/* tests/* DESCRIPTION NAMESPACE .Rbuildignore
	$(R) CMD build .

check: $(PACKAGE)
	$(R) CMD check $(PACKAGE)

check-cran: $(PACKAGE)
	@$(R-devel) -q -s -e 'stopifnot(`Please run Make with R-devel=path/to/R-devel`=grepl("devel", R.version$$status))'
	$(R-devel) CMD check --timings --as-cran $(PACKAGE)

install: $(PACKAGE)
	$(R) CMD INSTALL $(PACKAGE)

revdep-prepare:
	cd "$(REVDEPCHECK_DIRECTORY)" && $(R)script $(shell realpath revdepcheck/prepare.R)

revdepcheck/revdep-CRAN.rds: $(REVDEPCHECK_DIRECTORY)/*.tar.gz
	cd $(REVDEPCHECK_DIRECTORY) && \
		REVDEPCHECK_NCPUS=$(REVDEPCHECK_NCPUS) OMP_THREAD_LIMIT=1 \
		$(R)script $(shell realpath revdepcheck/run.R) XML $(shell realpath $@)

revdepcheck/revdep-new.rds: $(PACKAGE) $(REVDEPCHECK_DIRECTORY)/*.tar.gz
	cd $(REVDEPCHECK_DIRECTORY) && \
		REVDEPCHECK_NCPUS=$(REVDEPCHECK_NCPUS) OMP_THREAD_LIMIT=1 \
		$(R)script $(shell realpath revdepcheck/run.R) $(shell realpath $(PACKAGE)) $(shell realpath $@)

revdep-compare: revdepcheck/revdep-CRAN.rds revdepcheck/revdep-new.rds
	$(R)script revdepcheck/compare.R revdepcheck/revdep-CRAN.rds revdepcheck/revdep-new.rds
